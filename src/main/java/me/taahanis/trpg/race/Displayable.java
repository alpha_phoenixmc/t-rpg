package me.taahanis.trpg.race;

import org.bukkit.ChatColor;

public interface Displayable {
    
    public int getLevel();
    
    public String getPrefix();
    
    public String getRankName();
    
    public ChatColor getChatColor();
    
}
