/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.trpg;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 *
 * @author moh_a
 */
public class TParticles {
    
    public TRPG plugin;
    public TParticles(TRPG instance){
        plugin = instance;
    }
    
    public void fireworkSpell(final Player player)
    {
        new BukkitRunnable(){

            double t = 0; 
            
            @Override
            public void run() {
                Location playerLoc = player.getLocation();    
                t = t + 0.1*Math.PI;
                for(double theta = 0; theta <= 2*Math.PI; theta = theta + Math.PI/8){
                    double x = t*Math.cos(theta);
                    double y = Math.exp(-0.1*t) * Math.sin(t) + 1.5;
                    double z = t*Math.sin(theta);
                    playerLoc.add(x, y, z);
                    
                    for(Player anotherPlayer : plugin.getServer().getOnlinePlayers()){
                        anotherPlayer.spawnParticle(Particle.FIREWORKS_SPARK, playerLoc, 1);
                    }
                    playerLoc.subtract(x, y, z);
                }
                if(t > 20){
                    this.cancel();
                }
            }
            
        }.runTaskTimer(plugin, 1, 1);
    }
    
}
