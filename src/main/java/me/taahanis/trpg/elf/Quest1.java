package me.taahanis.trpg.elf;

import java.io.File;
import me.taahanis.trpg.TRPG;
import me.taahanis.trpg.TService;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;


public class Quest1 extends TService {

    public Quest1(TRPG plugin) {
        super(plugin);
    }
    
    File edFile = new File(plugin.getDataFolder(), "entitydata.yml");
    YamlConfiguration entityData = YamlConfiguration.loadConfiguration(edFile);
    
    @EventHandler
    public void entityDamage(EntityDamageByEntityEvent event)
    {
        Entity entity = event.getEntity();
        
        if (entity.getUniqueId().equals(entityData.getString("elf.quest1.villagerid")))
        {
            event.setCancelled(true);
            event.setDamage(0);
        }
        
    }
    @EventHandler
    public void readthetitle(EntityDamageEvent event)
    {
        Entity entity = event.getEntity();
        
        if (entity.getUniqueId().equals(entityData.getString("elf.quest1.villagerid")))
        {
            event.setCancelled(true);
            event.setDamage(0);
        }
        
    }
    
}
