package me.taahanis.trpg;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.taahanis.trpg.command.Command_checkrace;
import me.taahanis.trpg.command.Command_creative;
import me.taahanis.trpg.command.Command_race;
import me.taahanis.trpg.command.Command_reloadconfigs;
import me.taahanis.trpg.command.Command_save;
import me.taahanis.trpg.command.Command_setrank;
import me.taahanis.trpg.command.Command_spawnquest;
import me.taahanis.trpg.command.Command_tpo;
import me.taahanis.trpg.elf.Quest1;
import me.taahanis.trpg.listeners.ChooseYourRace;
import me.taahanis.trpg.listeners.RankListener;
import me.taahanis.trpg.race.RaceManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

public class TRPG extends JavaPlugin {
    
    public static TRPG plugin;
    //
    public File configFile;
    public File playerFile;
    public File edFile;
    
    public RankListener ral;
    public RaceManager rm;
    public ChooseYourRace cyr;
    
    //public RankList rl;
    public TRankFunctions trf = new TRankFunctions(this);
    public TParticles tp = new TParticles(this);
    public TPlayer tpl = new TPlayer(this);
    //
    public Quest1 elfq1;
    //
    public TAnnouncer tan;
    

    
    //
    
    @Override
    public void onEnable()
    {
            plugin = this;
            
            if (!plugin.getDataFolder().exists())
            {
                plugin.getDataFolder().mkdirs();
            }
            
            
            
            
            configFile = new File(plugin.getDataFolder(), "config.yml");
            playerFile = new File(plugin.getDataFolder(), "players.yml");
            edFile = new File(plugin.getDataFolder(), "entitydata.yml");
            
            if (!configFile.exists())
            {
                createDefaultConfig();
            }
            if (!playerFile.exists())
            {
                createPlayerFile();
            }
            if (!edFile.exists())
            {
                createEntityIDFile();
            }
            
            
            ral = new RankListener(this);
            rm = new RaceManager(this);
            cyr = new ChooseYourRace(this);
            //rl = new RankList(this);
            //
            elfq1 = new Quest1(this);
            //
            tan = new TAnnouncer(this);
            
            
            //
            plugin.getCommand("setrank").setExecutor(new Command_setrank());
            plugin.getCommand("race").setExecutor(new Command_race());
            plugin.getCommand("checkrace").setExecutor(new Command_checkrace());
            plugin.getCommand("tpo").setExecutor(new Command_tpo());
            plugin.getCommand("creative").setExecutor(new Command_creative());
            plugin.getCommand("save").setExecutor(new Command_save());
            plugin.getCommand("spawnquest").setExecutor(new Command_spawnquest());
            plugin.getCommand("reloadconfigs").setExecutor(new Command_reloadconfigs());
            YamlConfiguration config = YamlConfiguration.loadConfiguration(configFile);
            
            Bukkit.getLogger().info("T-RPG has loaded.");
            plugin.saveDefaultConfig();
        try {
            config.save(configFile);
        } catch (IOException ex) {
            Logger.getLogger(TRPG.class.getName()).log(Level.SEVERE, null, ex);
        }
        
 
        }
        
        

    public void createPlayerFile()
    {
        YamlConfiguration players = YamlConfiguration.loadConfiguration(playerFile);
        YamlConfiguration config = YamlConfiguration.loadConfiguration(configFile);
        try {
            config.save(configFile);
            players.save(playerFile);
        } catch (IOException ex) {
            Logger.getLogger(TRPG.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void createDefaultConfig()
    {
        List<String> defaultRanks = new ArrayList<String>();

    // add some default rank names/types
    defaultRanks.add("Owner");
    defaultRanks.add("Developer");
    defaultRanks.add("Admin");
    defaultRanks.add("Mod");
    defaultRanks.add("Helper");
    defaultRanks.add("Guest");

     // create section in config and set its value to our array
    plugin.getConfig().createSection("ranks");

    for(String s : defaultRanks){
        plugin.getConfig().createSection("ranks." + s);
        plugin.getConfig().set("ranks." + s + ".prefix", s);
    }
    for(String key : plugin.getConfig().getConfigurationSection("ranks").getKeys(false)){
        plugin.getConfig().set("ranks." + key + ".permissions", new ArrayList<String>());
        
    }
    
    plugin.getConfig().createSection("server");
    plugin.getConfig().set("server.elf.quest1", false);
    plugin.getConfig().set("server.elf.quest2", false);
    plugin.getConfig().set("server.elf.quest3", false);
    plugin.getConfig().set("server.human.quest1", false);
    plugin.getConfig().set("server.human.quest2", false);
    plugin.getConfig().set("server.human.quest3", false);
    //
    String prefix = "&8[&6T-RPG&8]";
    prefix = ChatColor.translateAlternateColorCodes('&', prefix);  
    String color = "&a";
    color = ChatColor.translateAlternateColorCodes('&', color);
    int delay = 600;
    plugin.getConfig().set("announcer.prefix", prefix);
    plugin.getConfig().set("announcer.enabled", true);
    plugin.getConfig().set("announcer.color", color);
    plugin.getConfig().set("announcer.delay", delay);
    plugin.getConfig().set("announcer.messages", new ArrayList<String>());
    //
    plugin.getConfig().set("server.ip", "insertserverip");
    plugin.getConfig().set("server.name", "AlphaRPG");
    
    
    
    
    
    
    
            plugin.saveConfig();
    }
    
    public void createEntityIDFile()
    {
        YamlConfiguration entityData = YamlConfiguration.loadConfiguration(edFile);
        entityData.set("elf.quest1.villagerid", "Unknown");
        entityData.set("elf.quest2.villagerid", "Unknown");
        entityData.set("elf.quest3.villagerid", "Unknown");
        //
        
        entityData.set("human.quest1.villagerid", "Unknown");
        entityData.set("human.quest2.villagerid", "Unknown");
        entityData.set("human.quest3.villagerid", "Unknown");
        try {
            entityData.save(edFile);
        } catch (IOException ex) {
            Logger.getLogger(TRPG.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    @Override
    public void onDisable()
    {
        saveYamls();
    }
    
    
    public void saveYamls()
    {
     plugin.saveDefaultConfig();
    }
  
    
  
   
    
}
