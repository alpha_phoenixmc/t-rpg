package me.taahanis.trpg.staff;

import java.io.File;
import me.taahanis.trpg.TRPG;
import me.taahanis.trpg.race.Displayable;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public enum Rank implements Displayable {
    
   NOTSTAFF(0, "a", "Guest", ChatColor.GREEN),
   HELPER(1, "a", "Helper", ChatColor.AQUA),
   MOD(2, "a", "Mod", ChatColor.LIGHT_PURPLE),
   ADMIN(3, "an", "Admin", ChatColor.RED),
   CONSOLE(4, "the", "Console", ChatColor.DARK_BLUE);
    public final int level;
    public final String pre;
    public final String prefix;
    public final ChatColor color;

    private Rank(int level, String pre, String prefix, ChatColor color) {
        this.level = level;
        this.pre = pre;
        this.prefix = prefix;
        this.color = color;
    }

    @Override
    public int getLevel() {
        return level;
    }

    @Override
    public String getPrefix() {
        return ChatColor.DARK_GRAY + "[" + color + prefix + ChatColor.DARK_GRAY + "] ";
    }

    @Override
    public String getRankName() {
        return prefix;
    }

    @Override
    public ChatColor getChatColor() {
        return color;
    }
    
    public static boolean doesRankExist(String rankName){
    for(String key : TRPG.plugin.getConfig().getConfigurationSection("ranks").getKeys(false)){
        if(key.equalsIgnoreCase(rankName)){
            return true;
        }
    }
    return false;
}
    
    public static Rank getRace(CommandSender player) {
        File playerFile = new File(TRPG.plugin.getDataFolder(), "players.yml");
         YamlConfiguration players = YamlConfiguration.loadConfiguration(playerFile);
        if (!(player instanceof Player)) {
            if ("Console".equalsIgnoreCase(player.getName())) {
                return Rank.CONSOLE;
            } else {
                OfflinePlayer offplayer = Bukkit.getOfflinePlayer(player.getName().replaceAll("[^A-Za-z0-9]", ""));
                if (offplayer == null) {
                    return Rank.NOTSTAFF;
                }
                for (Rank rank : Rank.values()) {
                    if (players.getString(offplayer.getName().toLowerCase() + ".rank").equalsIgnoreCase((rank.prefix))) {
                        return rank;
                    }
                }
                return Rank.ADMIN;
            }
        }
        try {
            for (Rank rank : Rank.values()) {
                if (players.getString(((Player) player).getName().toLowerCase() + ".rank").equalsIgnoreCase(rank.prefix)) {
                    return rank;
                }
            }
        } catch (Exception ignored) {
        }
        
        return Rank.NOTSTAFF;
    }
        
        public static boolean isRank(CommandSender player, int rank)
    {
        return getRace(player).level >= rank;
    }

    public static boolean isRank(CommandSender player, Rank rank)
    {
        return getRace(player).equals(rank);
    }

    
    // Ranks CommandSender continued..
    
    
    public static boolean isNotAStaff(Player p)
    {
        return getRace(p).level >= 0;
    }

    public static boolean isHelper(Player p) {
        return getRace(p).level >= 1;
    }

    public static boolean isMod(Player p) {
        return getRace(p).level >= 2;
    }

    public static boolean isAdmin(Player p) {
        return getRace(p).level >= 3;
    }

    public static boolean isConsole(Player p) {
        return getRace(p).level >= 4;
    }
    
    
    
    
}
    
  
