package me.taahanis.trpg.command;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.taahanis.trpg.TRPG;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class Command_setrank implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        File playerFile = new File(TRPG.plugin.getDataFolder(), "players.yml");
         YamlConfiguration players = YamlConfiguration.loadConfiguration(playerFile);
        
        if(sender instanceof Player){
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "You can only use this on console");
            return true;
        }
        if (args.length == 0)
        {
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "Correct usage is: /setrank <player> <rank>");
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "The races are: ");
            TRPG.plugin.getConfig().getStringList("races");
            return true;
        }
        
        Player target = Bukkit.getServer().getPlayer(args[0]);
        if (!players.contains(target.getName()))
        {
            sender.sendMessage(ChatColor.GOLD + "[TPRG] " + ChatColor.RED + "Player currently has not chosen their race yet. Sorry");
            return true;
        }
        
        
        String incRank = args[1];
        if(!TRPG.plugin.trf.doesRankExist(incRank)){
                       sender.sendMessage("Cannot setrank. The rank: " + incRank + " does not exist.");
                           return true;
                    }
        if (target == null)
        {
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "Player not found!");
            return true;
        }
        players.set(target.getName() + ".rank", incRank);
        Bukkit.broadcastMessage(ChatColor.RED + "CONSOLE - Setting " + target.getName() + " to " + incRank);
        try {
            players.save(playerFile);
        } catch (IOException ex) {
            Logger.getLogger(Command_setrank.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return true;
    }
    
}
