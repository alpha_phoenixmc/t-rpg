/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.trpg.command;

import java.io.File;
import me.taahanis.trpg.TRPG;
import me.taahanis.trpg.staff.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

/**
 *
 * @author moh_a
 */
public class Command_checkrace implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        File playerFile = new File(TRPG.plugin.getDataFolder(), "players.yml");
         YamlConfiguration players = YamlConfiguration.loadConfiguration(playerFile);
        final Player player = (Player) sender;
        if (!player.hasPermission("trpg.admin"))
        {
            sender.sendMessage(ChatColor.RED + "You must be an admin to do this!");
            return true;
        }
        if(!(sender instanceof Player)){
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "You can only use this in game");
            return true;
        }
        if (args.length == 0)
        {
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "Correct usage is: /checkrace <player>>");
            return true;
        }
        
        Player target = Bukkit.getServer().getPlayer(args[0]);
        if (target == null)
        {
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "Player not found!");
            return true;
        }
        String targetName = target.getName();
        
        player.sendMessage(ChatColor.RED + targetName + " is currently a " + players.getString(targetName + ".race"));
        
        
        
        return true;
    }
    
}
