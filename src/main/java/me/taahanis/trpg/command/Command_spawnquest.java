package me.taahanis.trpg.command;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.taahanis.trpg.TRPG;
import static me.taahanis.trpg.TRPG.plugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Command_spawnquest implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        
        File edFile = new File(plugin.getDataFolder(), "entitydata.yml");
        YamlConfiguration config = YamlConfiguration.loadConfiguration(edFile);
        
        if(!(sender instanceof Player)){
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "You can only use this in-game");
            return true;
        }
        if (args.length == 0)
        {
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "Correct usage is: /spawnquest <race1 | race2 | race3>");
            sender.sendMessage(ChatColor.RED + "Replace race with the race name.");
            return true;
        }
        
        final Player player = (Player) sender;
        if (!player.getName().equals("taahanis"))
        {
            sender.sendMessage(ChatColor.GOLD + "[TPRG] " + ChatColor.RED + "Only taahanis may use this command.");
            return true;
        }
        switch (args[0])
        {
            case "elf1":
            {
                if (!TRPG.plugin.getConfig().getBoolean("server.elf.quest1", true))
                {
                    sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "Elf Quest1 is currently disabled.");
                    return true;
                } else {
                Villager villager = (Villager) player.getLocation().getWorld().spawnEntity(player.getLocation(), EntityType.VILLAGER);
                villager.setCustomName(ChatColor.RED + "Elf - Quest #1");
                villager.setCustomNameVisible(true);
                villager.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 10000000, 10000000));
                config.set("elf.quest1.villagerid", villager.getUniqueId().toString());
            try {
                config.save(edFile);
            } catch (IOException ex) {
                Logger.getLogger(Command_spawnquest.class.getName()).log(Level.SEVERE, null, ex);
            }
                sender.sendMessage(ChatColor.RED + "Mob spawned.");
                }
                break;
            }
            default:
                sender.sendMessage(ChatColor.RED + "Correct usage is /spawnquest <race1 | race2 | race3>");
                sender.sendMessage(ChatColor.RED + "Replace race with the race name.");
        }
        
        return true;
    }
    
}
