package me.taahanis.trpg.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_tpo implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        final Player player = (Player) sender;
        if (!(sender instanceof Player))
        {
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "This is an in-game only command.");
            return true;
        }
        if (args.length == 0)
        {
            player.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "Correct usage: /tpo <player>");
            return true;
        }
        
        if (!player.hasPermission("trpg.admin"))
        {
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "You have no permission. Must be an admin.");
            return true;
        }
        final Player target = Bukkit.getServer().getPlayer(args[0]);
        
        if (target == null)
        {
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "Player not found!");
            return true;
        }
        
        player.teleport(target);
        player.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "You teleported to " + target.getName());
        
        return true;
    }
    
}
