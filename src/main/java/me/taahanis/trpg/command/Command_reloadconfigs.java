/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.trpg.command;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.taahanis.trpg.TRPG;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

/**
 *
 * @author moh_a
 */
public class Command_reloadconfigs implements CommandExecutor {
    
     @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        File configFile = new File(TRPG.plugin.getDataFolder(), "config.yml");
         YamlConfiguration config = YamlConfiguration.loadConfiguration(configFile);
         
         File playersFile = new File(TRPG.plugin.getDataFolder(), "players.yml");
         YamlConfiguration players = YamlConfiguration.loadConfiguration(playersFile);
         
         File edFile = new File(TRPG.plugin.getDataFolder(), "entitydata.yml");
         YamlConfiguration entityData = YamlConfiguration.loadConfiguration(edFile);
        
        if(sender instanceof Player){
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "You can only use this on console");
            return true;
        }
        if (args.length == 0)
        {
            try {
                config.save(configFile);
                players.save(playersFile);
                entityData.save(edFile);
            } catch (IOException ex) {
                Logger.getLogger(Command_reloadconfigs.class.getName()).log(Level.SEVERE, null, ex);
            }
                
            try {
                config.load(configFile);
                players.load(playersFile);
                entityData.load(edFile);
            } catch (IOException ex) {
                Logger.getLogger(Command_reloadconfigs.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidConfigurationException ex) {
                Logger.getLogger(Command_reloadconfigs.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            Bukkit.getLogger().info("Configs reloaded.");
                
        }
        return true;
    }
           
    
}
