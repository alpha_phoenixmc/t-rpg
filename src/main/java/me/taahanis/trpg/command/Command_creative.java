package me.taahanis.trpg.command;

import me.taahanis.trpg.staff.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Command_creative implements CommandExecutor {
    
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        final Player player = (Player) sender;
    if(!(sender instanceof Player)){
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "You can only use this in-game");
            return true;
        }
    
        if (args.length == 0)
        {
            player.setGameMode(GameMode.CREATIVE);
            player.sendMessage(ChatColor.RED + "Your gamemode has been set to " + ChatColor.AQUA + "Creative.");
            return true;
        }
        if (!player.hasPermission("trpg.admin"))
        {
             player.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "You must be an Admin to set everyone to creative.");
             return true;
        }
        if (args[0].equalsIgnoreCase("-a"))
        {
            for (Player player1 : Bukkit.getServer().getOnlinePlayers())
            {
                player1.setGameMode(GameMode.CREATIVE);
               
                
                return true;
            }
            Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " - Setting everyone's gamemode to " + ChatColor.GREEN + "Creative");
            return true;
        }
        return true;
    }
}

    

