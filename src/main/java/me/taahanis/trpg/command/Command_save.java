/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.trpg.command;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.taahanis.trpg.TRPG;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author moh_a
 */
public class Command_save implements CommandExecutor {
   
    
        @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
         File playerFile = new File(TRPG.plugin.getDataFolder(), "players.yml");
         YamlConfiguration players = YamlConfiguration.loadConfiguration(playerFile);
                 
        if(sender instanceof Player){
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "You can only use this on console");
            return true;
        }
        if (!sender.hasPermission("trpg.admin"))
        {
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "No permission.");
            return true;
        }
        if (args.length == 0)
        {
            
            TRPG.plugin.saveConfig();
             try {
                 players.save(playerFile);
             } catch (IOException ex) {
                 Logger.getLogger(Command_save.class.getName()).log(Level.SEVERE, null, ex);
             }
            sender.sendMessage(ChatColor.RED + "Configs saved.");
            return true;
        }
        return true;
    }
    
}
