package me.taahanis.trpg.command;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.taahanis.trpg.TRPG;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class Command_race implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        File playerFile = new File(TRPG.plugin.getDataFolder(), "players.yml");
         YamlConfiguration players = YamlConfiguration.loadConfiguration(playerFile);
        if(!(sender instanceof Player)){
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "You can only use this in-game");
            return true;
        }
        if (args.length == 0)
        {
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "Correct usage is: /race <Human | Elf>");
            return true;
        }
        
        final Player player = (Player) sender;
        if (!player.hasPermission("trpg.admin"))
        {
            if (players.contains(player.getName()))
            {
            sender.sendMessage(ChatColor.GOLD + "[TRPG] " + ChatColor.RED + "You've already chosen your race. Only admins may re-choose their race.");
            return true;
            }
            
        }
        
        switch (args[0])
        {
            case "Human":
            {
                players.set(player.getName() + ".ip", player.getAddress().getHostString());
                players.set(player.getName() + ".race", "Human");
                players.set(player.getName() + ".rank", TRPG.plugin.trf.getLowestRank());
                    try {
                        players.save(playerFile);
                    } catch (IOException ex) {
                        Logger.getLogger(Command_race.class.getName()).log(Level.SEVERE, null, ex);
                    }
                sender.sendMessage(ChatColor.RED + "You are now a Human. We recommend relogging for your health to set.");
                break;
            }
            case "Elf":
            
            {
                
                players.set(player.getName() + ".ip", player.getAddress().getHostString());
                players.set(player.getName() + ".race", "Elf");
                players.set(player.getName() + ".rank", TRPG.plugin.trf.getPlayerRank(player));
                    try {
                        players.save(playerFile);
                    } catch (IOException ex) {
                        Logger.getLogger(Command_race.class.getName()).log(Level.SEVERE, null, ex);
                    }
                sender.sendMessage(ChatColor.RED + "You are now an Elf. We recommend relogging for your health to set.");
                break;
                
            }
            
            default:
                sender.sendMessage(ChatColor.GOLD + "[TPRG] " + ChatColor.RED + "The only races available are Human and Elf.");
        }
        
        return true;
    }
    
}
