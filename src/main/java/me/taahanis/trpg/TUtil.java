/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.trpg;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import org.bukkit.ChatColor;

public class TUtil {
    
    public static final List<ChatColor> COLORS = Arrays.asList(
            ChatColor.DARK_BLUE,
            ChatColor.DARK_GREEN,
            ChatColor.DARK_AQUA,
            ChatColor.DARK_RED,
            ChatColor.DARK_PURPLE,
            ChatColor.GOLD,
            ChatColor.BLUE,
            ChatColor.GREEN,
            ChatColor.AQUA,
            ChatColor.RED,
            ChatColor.LIGHT_PURPLE,
            ChatColor.YELLOW
    );
    public static Random random = new Random();
    
    public static ChatColor randomChatColour() {
        return COLORS.get(random.nextInt(COLORS.size()));
    }

    public static ChatColor randomChatColor() {
        return randomChatColour();
    }
    public static String colorize(String string) {
        string = ChatColor.translateAlternateColorCodes('&', string);
        string = string.replaceAll("&-", randomChatColour().toString());
        return string;
    }
    

    
}
