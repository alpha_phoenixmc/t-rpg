/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.trpg;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.event.Listener;

public abstract class TRPGService implements Listener {

    protected final TRPG plugin;
    protected final Server server;

    @SuppressWarnings("LeakingThisInConstructor")
    public TRPGService(TRPG plugin) {
        this.plugin = plugin;
        this.server = plugin.getServer();
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }
}