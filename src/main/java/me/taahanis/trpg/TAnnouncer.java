/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.trpg;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 *
 * @author moh_a
 */
public class TAnnouncer extends TService {

    public TAnnouncer(TRPG plugin) {
        super(plugin);
    }
    
    private void start(Plugin pl) {
        if (plugin.getConfig().getBoolean("announcer.enabled")) {
            new BukkitRunnable() {
                int number = 0;

                @Override
                public void run() {
                    if (this.number >= plugin.getConfig().getStringList("announcer.messages").size()) {
                        this.number = 0;
                    }

                    final String prefix = plugin.getConfig().getString("announcer.prefix");
                    final String message_color = plugin.getConfig().getString("announcer.color");
                    final String message = plugin.getConfig().getStringList("announcer.messages").get(this.number);

                    this.number += 1;

                    for (Player player : server.getOnlinePlayers()) {
                        player.sendMessage(TUtil.colorize(prefix + " " + message_color + message));
                    }
                }
            }.runTaskTimer(pl, 100L, 20 * pl.getConfig().getInt("announcer.delay"));
        }
    }
    
}
