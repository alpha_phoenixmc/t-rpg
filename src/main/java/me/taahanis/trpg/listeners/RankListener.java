package me.taahanis.trpg.listeners;

import me.taahanis.trpg.TActionBar;
import me.taahanis.trpg.TRPG;
import me.taahanis.trpg.TService;
import me.taahanis.trpg.TUtil;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerAnimationType;
import static org.bukkit.event.player.PlayerAnimationType.ARM_SWING;
import org.bukkit.event.player.PlayerJoinEvent;

public class RankListener extends TService {
    
    
    public RankListener(TRPG plugin)
    {
        super(plugin);
    }
        
        @EventHandler
    public void onChat(AsyncPlayerChatEvent event)
    {
    Player player = event.getPlayer();
    String playerName;
    playerName = player.getName();
    String message = event.getMessage();
    message = net.md_5.bungee.api.ChatColor.translateAlternateColorCodes('&', message);
    

    String message1 = event.getMessage();
    String playersRank = plugin.trf.getPlayerRank(player);
    String prefixToUse = plugin.trf.getRankPrefix(playersRank);
    String chat = org.bukkit.ChatColor.DARK_GRAY + "[" + net.md_5.bungee.api.ChatColor.translateAlternateColorCodes('&', prefixToUse) + net.md_5.bungee.api.ChatColor.DARK_GRAY + "] " + net.md_5.bungee.api.ChatColor.WHITE + "<" + net.md_5.bungee.api.ChatColor.RED + playerName + net.md_5.bungee.api.ChatColor.WHITE + "> " + message;
    
    event.setFormat(chat);
    }
    
    @EventHandler
    public void onJoin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();
        plugin.trf.setPlayerBukkitPerms(player);
        
        for (Player player1 : Bukkit.getOnlinePlayers())
        {
            if (player.getName().equals("taahanis"))
        {
            TActionBar.sendHotBarMessage(player1, ChatColor.RED + "taahanis has joined!");
        }
            
        }
    }


    
}
