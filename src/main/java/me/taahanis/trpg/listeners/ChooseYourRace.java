package me.taahanis.trpg.listeners;

import java.io.File;
import me.taahanis.trpg.TRPG;
import me.taahanis.trpg.TService;
import me.taahanis.trpg.staff.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;

public class ChooseYourRace extends TService {
    
    public final static String GUIName = ChatColor.AQUA + "Choose your Race";
    
    public static Inventory myInventory = (Inventory) Bukkit.createInventory(null, 18, GUIName);

    
    public ChooseYourRace(TRPG plugin) {
        super(plugin);
    }
    


    @EventHandler
    public void chooseRace(PlayerMoveEvent event)
    {
        File playerFile = new File(TRPG.plugin.getDataFolder(), "players.yml");
         YamlConfiguration players = YamlConfiguration.loadConfiguration(playerFile);
        final String player = event.getPlayer().getName();
        final Player player1 = event.getPlayer();
        if (!players.contains(player))
        {
         plugin.tpl.setFrozen(player1);
            player1.sendMessage(ChatColor.RED + "You must choose a race! Do /race to choose!");
        } else if (players.contains(player))
                {
                    return;
                }
    }
    
    
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    
            
    {
        final Player player = event.getPlayer();
        

    }
    
    
}
