/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.trpg;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

/**
 *
 * @author moh_a
 */
public class TRankFunctions {
    
    public TRPG plugin;
    public TRankFunctions(TRPG instance){
        plugin = instance;
    }

    
    public String getRankPrefix(String rankName){
    return plugin.getConfig().getString("ranks." + rankName + ".prefix");
}
    public String getPlayerRank(Player player) {
        File playerFile = new File(plugin.getDataFolder(), "players.yml");
        YamlConfiguration players = YamlConfiguration.loadConfiguration(playerFile);
    return players.getString(player.getName() + ".rank");
    }
    
    public String getRankName(String rankName){
    for(String key : plugin.getConfig().getConfigurationSection("ranks").getKeys(false)){
        if(key.equalsIgnoreCase(rankName)){
            return key;
        }
    }
    return null;
}
    
    public boolean doesRankExist(String rankName){
    for(String key : plugin.getConfig().getConfigurationSection("ranks").getKeys(false)){
        if(key.equalsIgnoreCase(rankName)){
            return true;
        }
    }
    return false;
}
    public boolean setRank(String playerName, String rankName){
        File playerFile = new File(plugin.getDataFolder(), "players.yml");
        YamlConfiguration players = YamlConfiguration.loadConfiguration(playerFile);
    plugin.getConfig().set(playerName + ".rank", getRankName(rankName));
        try {
            players.save(playerFile);
        } catch (IOException ex) {
            Logger.getLogger(TRankFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    return false;
}
    
    public String getLowestRank(){
    String rank = "Unknown";
    for(String key : plugin.getConfig().getConfigurationSection("ranks").getKeys(false)){
        rank = key;
    }
    return rank;
}
    public List<String> getPermissionsForRank(String rankName){
    return plugin.getConfig().getStringList("ranks." + rankName + ".permissions");
}
    
    public void setPlayerBukkitPerms(Player player){
        
        String playerUUID = player.getUniqueId().toString();
    File playerFile = new File(plugin.getDataFolder() + File.separator + "players", playerUUID + ".yml");
    FileConfiguration playerYml = YamlConfiguration.loadConfiguration(playerFile);
        // create permissionAttachment for the player we can use
        PermissionAttachment perms = player.addAttachment(TRPG.plugin);
        // give the permission
        perms.setPermission("example.perm", true);
        
        // get a LIST of all the perms this player should have
List<String> permList = getPermissionsForRank(getPlayerRank(player));


// loop thru the list adding each perm
for(String perm : permList){

    // set the incoming string (perm) with bukkit to the Permission Attachment
    perms.setPermission(perm, true);

}

// the
    }
}
